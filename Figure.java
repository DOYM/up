public abstract class Figure {
private int centerX;
private int centerY;

public Figure(int centerX, int centerY) {
this.centerX = centerX;
this.centerY = centerY;
}

public abstract double square();

public int getQuadrant() {
if (centerX > 0) {
if (centerY > 0) {
return 1;
} else {
return 4;
}
} else {
if (centerY > 0) {
return 2;
} else {
return 3;
}
}
}
}