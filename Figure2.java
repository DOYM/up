public class Main {

public static void main(String[] args) {
Figure[] figures = new Figure[5];
double j;
for (int i = 0; i < 5; i++) {
j = Math.random();
if (j > 0.5) {
figures[i] = new Circle((int) (Math.random() * 100), (int) (Math.random() * 100));
} else {
figures[i] = new Rectangle((int) (Math.random() * 100), (int) (Math.random() * 100));
}
System.out.println("Фигура под номером " + (i + 1) + " - " + ((j > 0.5) ? "круг." : "прямоугольник.") + " Её площадь - " + figures[i].square());
}
}
}